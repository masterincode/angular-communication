import { Component, OnInit, ViewChild } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './product.service';
import { NgModel } from '@angular/forms';
import { CriteriaComponent } from '../shared/criteria/criteria.component';
import { ProductParameterService } from './product-parameter.service';
@Component({
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
     /*
    Notes: Demerit
    1. ViewChild reference not available until AfterViewInit
    2. ViewChild reference not available, If the element is not in the DOM
    */

    //This is for ngModel data structure , we dont get acces to native element
    @ViewChild(NgModel) filterInput: NgModel;  
    // @ViewChildren(NgModel) inputElementRefs: QueryList<ElementRef>;

    @ViewChild(CriteriaComponent) criteriaComponent: CriteriaComponent;
    parentListFilter:string;

    constructor(private productService: ProductService,private productParameterService: ProductParameterService) { }
    pageTitle: string = 'Product List';
    imageWidth: number = 50;
    imageMargin: number = 2;
    errorMessage: string;
    filteredProducts: IProduct[];
    products: IProduct[];
    includeDetail: boolean = true;
   
    get showImage(): boolean {
        return this.productParameterService.showImage;
    }

    set showImage(value: boolean ) {
        this.productParameterService.showImage = value;
    }

    ngOnInit(): void {
        this.productService.getProducts().subscribe(
            (products: IProduct[]) => {
                this.products = products;
                this.criteriaComponent.listFilter = this.productParameterService.filterBy;
            },
            (error: any) => this.errorMessage = <any>error
        );
    }

    onValueChange($event: string): void {
        this.productParameterService.filterBy = $event ;
        this.performFilter($event);
    }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    performFilter(filterBy?: string): void {
        if (filterBy) {
            this.filteredProducts = this.products.filter((product: IProduct) =>
                product.productName.toLocaleLowerCase().indexOf(filterBy.toLocaleLowerCase()) !== -1);
        } else {
            this.filteredProducts = this.products;
        }
    }
}
