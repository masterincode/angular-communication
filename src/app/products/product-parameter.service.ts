import { Injectable, OnDestroy } from '@angular/core';

@Injectable()
export class ProductParameterService implements OnDestroy{
  constructor() {
    console.log('Service Created');
   }
  showImage: boolean;
  filterBy:string;

  ngOnDestroy(): void {
    console.log('Service Destoryed');
  }

}