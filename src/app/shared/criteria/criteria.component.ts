import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input, OnChanges, SimpleChanges,EventEmitter, Output } from '@angular/core';
import { NgModel } from '@angular/forms';


@Component({
  selector: 'pm-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.css']
})
export class CriteriaComponent implements OnInit,OnChanges ,AfterViewInit  {
 /* Notes: Demerit
    1. ViewChild reference not available until AfterViewInit
    2. ViewChild reference not available, If the element is not in the DOM
  */
    //This is for to access native element properties
    @ViewChild('filterElement') filterElementRef: ElementRef ;
    @ViewChild(NgModel) inputFilter: NgModel;
    @Input() displayDetail: boolean;
    @Input() hitCount:number;
    
    hitMessage:string;
    @Output()  valueChange: EventEmitter<string> = new EventEmitter<string>();
    private _listFilter: string;

    get listFilter(): string {
      return this._listFilter;
    }
    set listFilter(value: string) {
      this._listFilter = value;
      this.valueChange.emit(value);
    }

    constructor() { }
  
  ngOnInit() {
   
  }

  ngOnChanges(changes : SimpleChanges): void {
    if (changes['hitCount'] && !changes['hitCount'].currentValue) {
      this.hitMessage = 'No matches found';
    } else {
      this.hitMessage = 'Hits:' + this.hitCount;
    }
  }

  ngAfterViewInit(): void {
    if(this.filterElementRef.nativeElement)
    this.filterElementRef.nativeElement.focus();
}

}
