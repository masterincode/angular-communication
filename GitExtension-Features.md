# GitExtension Features

- Rewrite commit message
- Single file History and Blame
- Staged to particular piece of code

## Revert Operation

- Revert commit of commited file. Note: This operation will also make revert and commit.

## Reset Operation (Soft, Mixed and Hard)

- Reset one file to(A or B) when u have bulk commit . Note: No need to reset whole commit.
- Reset all commits from selected commit(Soft, Mixed and Hard) and also get back to same commit "git reset HEAD@{1}".
  Note: after reset do not commit else u wont get back to previous all commits

    git reset HEAD@{1}

## Branch

- create branch from a particular branch(Right click -> create branch)
- Orphan branch 
- Rename branch
- Checkout (Right Click -> Checkout branch) or From above Dropdown (Select branch)
- Delete branch (Force Delete)
- Stash and Stash Pop operations 
- Cherry pick (2 ways to do)
- Merge and rebase branch

## Release ,Pathces and Tags

- Release is a branch naming release v1.0
- create Tags
- create patch and apply patch 

